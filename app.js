const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const path = require('path');
const app = express();
const dotEnv = require("dotenv");
const router = require("./controllers/route");

dotEnv.config({path: "./config.env"});

app.set('views',path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
app.use("/", router);

app.listen(process.env.PORT || 3000, _=>{
    console.log(`Servidor iniciado ${process.env.PORT}`);
});