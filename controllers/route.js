const knex = require("../knex");
const express = require("express");
const router = express.Router();

router.get("/", async (req, res) => {
    try{
        const resultado = await knex.select().from("tbl_tareas");
        res.render("index", {
            selectResultado: resultado
        });
    } catch(error){
        console.log(error);
    }
});

router.post("/registrarEtrada", async (req, res) => {

    const {tituloInput, cuerpoInput} = req.body;
    try{
        await knex("tbl_tareas").insert({
            titulo: tituloInput,
            cuerpo: cuerpoInput
        });
    } catch(error){
        console.log(error);
    }
    res.redirect("/");
});

router.get("/registrarEtrada", (req, res) => {
    res.render("entradas");
});

router.get("/borrar/:idBorrar", async (req, res) => {
    const id = req.params.idBorrar;
    try{
        await knex("tbl_tareas").where("id_tarea", id).del();
    } catch(error){
        confirm.log(error);
    }
    res.redirect("/");
});

router.get("/editarEntrada/:idEditar", async (req, res) =>{
    const id = req.params.idEditar;
    try{
        const editarSelect = await knex.select().from("tbl_tareas").where("id_tarea", id);
        const posicionActual = editarSelect[0];
        console.log(editarSelect);
        res.render("editarEntrada", {
            objetoObtenido : posicionActual
        });
    } catch(error){
        console.log(error);
    }
});

router.post("/editarEntrada/:idEditar", async (req, res) =>{
    const id = req.params.idEditar;
    const {tituloInput, cuerpoInput} = req.body;
    try{
        await knex("tbl_tareas").where("id_tarea", id).update({
            titulo: tituloInput,
            cuerpo: cuerpoInput
        });
    } catch(error){
        console.log(error);
    }
    res.redirect("/");
});

module.exports = router;